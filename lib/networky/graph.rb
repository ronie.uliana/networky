# frozen_string_literal: true

module Networky
  class Graph
    attr_reader :properties

    def initialize(number_of_nodes = 0)
      @links = Hash.new do |h1, k1|
        h1[k1] = Hash.new { |h2, k2| h2[k2] = 0 }
      end
      @properties = Hash.new { |h, k| h[k] = [] }
      @node_counter = number_of_nodes
      @link_counter = 0
    end

    def include_node?(node)
      node < @node_counter
    end

    def add_node
      @node_counter += 1
    end

    def node(index)
      index < @node_counter ? Node.new(self, index) : nil
    end

    def nodes
      return enum_for(:nodes) unless block_given?
      @node_counter.times do |i|
        yield Node.new(self, i)
      end
    end

    def nodes_count
      @node_counter
    end

    def include_link?(source, target)
      @links[source].key?(target)
    end

    def add_link(source, target)
      return if include_link?(source, target)
      @links[source][target] = @link_counter
      @links[target][source] = @link_counter
      @node_counter = (source > target ? source : target) + 1
      @link_counter += 1
    end

    def links
      return enum_for(:links) unless block_given?
      visited = Array.new(@link_counter, false)
      @links.each do |source, targets|
        targets.each do |target, link_id|
          next if visited[link_id]
          visited[link_id] = true
          yield Link.new(self, source, target)
        end
      end
    end

    def links_count
      @link_counter
    end
  end
end
