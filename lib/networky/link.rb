# frozen_string_literal: true

module Networky
  class Link
    def initialize(graph, source, target)
      @graph = graph
      @source, @target = source < target ? [source, target] : [target, source]
    end

    def index
      [@source, @target]
    end

    def source
      Node.new(@graph, @source)
    end

    def target
      Node.new(@graph, @target)
    end
  end
end
