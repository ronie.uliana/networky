# frozen_string_literal: true

module Networky
  # Lightweight class that represents a node in the graph.
  #
  # It contains a reference to the graph itself. Consider it a "view"
  # of the graph instead of a full independent object.
  class Node
    attr_reader :index

    def initialize(graph, index)
      @graph, @index = graph, index
    end

    def []=(property, value)
      @graph.properties[property][index] = value
    end

    def [](property)
      @graph.properties[property][index]
    end
  end
end
