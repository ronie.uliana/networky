# frozen_string_literal: true

module Networky
  RSpec.describe Graph do
    subject { Graph.new }

    it "starts as an empty graph" do
      expect(subject.links.count).to eq(0)
      expect(subject.nodes.count).to eq(0)
    end

    describe "#add_node" do
      it "builds nodes" do
        subject.add_node
        expect(subject.nodes.count).to eq(1)
        expect(subject.nodes_count).to eq(1)

        subject.add_node
        expect(subject.nodes.count).to eq(2)
        expect(subject.nodes_count).to eq(2)
      end
    end

    describe "#add_link" do
      it "builds links" do
        subject.add_link(0, 0)
        expect(subject.links.count).to eq(1)
        expect(subject.links_count).to eq(1)
        expect(subject.nodes.count).to eq(1)

        subject.add_link(0, 1)
        expect(subject.links.count).to eq(2)
        expect(subject.links_count).to eq(2)
        expect(subject.nodes.count).to eq(2)
      end

      it "increases the number of nodes" do
        subject.add_link(0, 10)
        expect(subject.nodes.count).to eq(11)
      end

      it "silently ignores repeated links" do
        subject.add_link(0, 1)
        expect(subject.links.count).to eq(1)
        expect(subject.links_count).to eq(1)

        subject.add_link(0, 1)
        expect(subject.links.count).to eq(1)
        expect(subject.links_count).to eq(1)
      end

      it "is bidirectional" do
        subject.add_link(0, 1)
        subject.add_link(1, 0)
        expect(subject.links.count).to eq(1)
      end
    end

    describe "#node" do
      it "access specific node by index" do
        subject.add_node
        subject.add_node

        expect(subject.node(0).index).to eq(0)
        expect(subject.node(1).index).to eq(1)
        expect(subject.node(2)).to be_falsey
      end
    end

    describe "#nodes" do
      it "iterate over nodes" do
        subject.add_node
        subject.add_node
        subject.add_node

        nodes = subject.nodes
        expect(nodes.next.index).to eq(0)
        expect(nodes.next.index).to eq(1)
        expect(nodes.next.index).to eq(2)
        expect { nodes.next }.to raise_exception(StopIteration)
      end
    end

    describe "#links" do
      it "iterate over links" do
        subject.add_link(0, 1)
        subject.add_link(1, 2)
        subject.add_link(0, 2)

        links = subject.links.to_a
        expect(links.size).to eq(3)
        expect(links.map(&:index)).to include([0, 1], [1, 2], [0, 2])
      end
    end
  end
end
