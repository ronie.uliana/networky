# frozen_string_literal: true

module Networky
  RSpec.describe Node do
    let(:graph) { Graph.new(3) }

    describe "#[]= and #[]" do
      it "sets and recover node properties" do
        node = graph.node(0)
        node["test"] = 42

        graph.node(2)["test"] = 12
        graph.node(1)["other"] = "testing"

        expect(graph.node(0)["test"]).to eq(42)
        expect(graph.node(1)["test"]).to be_nil
        expect(graph.node(1)["other"]).to eq("testing")
        expect(graph.node(2)["test"]).to eq(12)
      end
    end
  end
end
