# frozen_string_literal: true

module Networky
  RSpec.describe Networky do
    it "has a version number" do
      expect(Networky::VERSION).not_to be nil
    end
  end
end
